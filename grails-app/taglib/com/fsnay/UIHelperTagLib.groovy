package com.fsnay

import com.fsnay.gt.AppUtils

class UIHelperTagLib {
    static namespace = "UIHelper"

    AuthService authService
    ContactGroupService contactGroupService

    def errorMessage = { attrs, body ->
        def model = attrs.model
        String fieldName = attrs.fieldName
        String errorMessage = attrs.errorMessage? g.message(code:attrs.errorMessage): g.message(code:"invalid.input")
        if(model && model.errors && model.errors.getFieldError(fieldName)){
            out << "<small class='form-text text-danger'><strong>${errorMessage}</strong></small>"
        }
    }

    def memberActionMenu = { attrs, body ->
        out << '<li class="nav-item dropdown show">'
        out << g.link(class: "nav-link dropdown-toggle", "data-toggle": "dropdown") { authService.getMemberName() }
        out << '<div class="dropdown-menu">'
        out << g.link(class: "dropdown-item", controller: "authentication", action: "logout") { g.message(code: "logout") }
        out << '</div></li>'
    }

    def leftNavigation = { attrs, body ->
        List navigations = [
                [controller: "dashboard", action: "index", name: "Dashboard"],
                [controller: "contactGroup", action: "index", name: "Contacts Group"],
                [controller: "contact", action: "index", name: "Contacts"],
        ]

        if(authService.isAdmin()){
            navigations.add([controller: "member", action: "index", name: "member"])
        }

        navigations.each { menu ->
            out << '<li class="list-group-item bg-dark shadow-sm">'
            out << g.link(controller: menu.controller, action: menu.action, class:"text-light") { g.message(code: menu.name, args: ['']) }
            out << '</li>'
        }
    }

    def contactGroup = { attrs, body ->
        String name = attrs.name ?: "contactGroups"
        out << g.select(class:"form-control", multiple: "multiple", optionValue: "name", optionKey: "id",
                value: attrs.value, name: name, from: contactGroupService.getGroupList())
    }

    def contactType = { attrs, body ->
        String name = attrs.name ?: "type"
        String value = attrs.value ?: ""
        def select = [:]
        select.HOME = "Home"
        select.PERSONAL = "Personal"
        select.OTHER = "Other"
        out << g.select(from:select, name:name,optionKey: "key",optionValue: "value", value:value,
                class:"form-control")
    }

    def appBaseURL = { attrs, body ->
        out << AppUtils.appBaseURL()
    }
}