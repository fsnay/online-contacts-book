package com.fsnay.gt

import com.fsnay.AppInitService

class BootStrap {

    def init = { servletContext ->
        AppInitService.initialize()
    }
    def destroy = {
    }
}
