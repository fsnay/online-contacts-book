package com.fsnay

import com.fsnay.gt.AppUtils
import com.fsnay.gt.Contact
import com.fsnay.gt.ContactDetails


class ContactDetailsService {

    private def getContactDetailsParamsParse( def params, Integer id = null) {

        def map = [
                id     : null,
                mobile : null,
                phone  : null,
                email  : null,
                website: null,
                address: null,
                type   : null,
                contact: null,
        ]

        if (id == null && params.detailsId) {
            map.id = params.detailsId
        }else if (id != null){
            try{
                if (params["detailsId"][id]){
                    map.id = params["detailsId"][id]
                }
            }catch(Exception e){}
        }

        if (id == null && params.mobile) {
            map.mobile = params.mobile
        }else if (id != null && params["mobile"][id]){
            map.mobile = params["mobile"][id]
        }

        if (id == null && params.phone) {
            map.phone = params.phone
        }else if (id != null && params["phone"][id]){
            map.phone = params["phone"][id]
        }

        if (id == null && params.email) {
            map.email = params.email
        }else if (id != null && params["email"][id]){
            map.email = params["email"][id]
        }

        if (id == null && params.website) {
            map.website = params.website
        }else if (id != null && params["website"][id]){
            map.website = params["website"][id]
        }

        if (id == null && params.address) {
            map.address = params.address
        }else if (id != null && params["address"][id]){
            map.address = params["address"][id]
        }

        if (id == null && params.type) {
            map.type = params.type
        }else if (id != null && params["type"][id]){
            map.type = params["type"][id]
        }

        return map
    }

    private def saveOrUpdate(def map,Contact contact){

        def validDetails = validateDetails(map)

        if(validDetails) {
            ContactDetails contactDetails
            try {
                if (map && map.id) {
                    contactDetails = get(map.id) ?: new ContactDetails()
                    contactDetails.properties = map
                    contactDetails.contact = contact
                } else {
                    map.contact = contact
                    contactDetails = new ContactDetails(map)
                }
                contactDetails.save(flush: true, failOnError: true)
                if (contactDetails.hasErrors()) {
                    println(contactDetails.errors)
                    log.error(contactDetails.errors)
                }
            }
            catch (Exception e ) {
                println(e.message)
                log.error(e.message)
            }
        }
    }


    def createOrUpdateDetails(Contact contact, def params) {

        if (params.type instanceof String) {
            saveOrUpdate(getContactDetailsParamsParse(params),contact)
        } else if (params.type && params.type.getClass().isArray()) {
            Integer index = 0
            params.type.each {
                saveOrUpdate(getContactDetailsParamsParse(params, index),contact)
                index++
            }
        }
    }

    def get(Serializable id){
        return ContactDetails.get(id)
    }

    def delete(Serializable id){
        ContactDetails contactDetails = get(id)
        if(contactDetails){
            contactDetails.delete(flush:true)
            return AppUtils.infoMessage("Contact details deleted successfully")
        }
        return AppUtils.infoMessage("An error occurred, please try again")
    }

    def getContactDetailsByContact(Contact contact){
        if(contact){
            return ContactDetails.createCriteria().list{
                eq("contact",contact)
            }
        }
        return []
    }

    private boolean validateDetails(def map){

        def mapSize = map.findAll{it.value != null && it != "type"}.size()
        boolean valid = true

//      indicates details values were erased in form but the details itself were not deleted,
//      and thus will be deleted now
        if(mapSize == 1 && map.id){
            delete(map.id)
            valid = false
        }
//        indicates details values were either empty or null, therefore must not be save
//        map.type is the only field with default value (HOME), but should not be saved by itself

        else if(mapSize == 0 || (mapSize == 1 && map.type) ){
            valid = false
        }
        return valid
    }


}
