package com.fsnay

import com.fsnay.gt.AppUtils
import com.fsnay.gt.GlobalConfig
import com.fsnay.gt.Member


class AuthService {

   private static final String AUTHORIZED = "AUTHORIZED"

    def setMemberAuthorization(Member member){
        def auth = [isLoggedin:true,member:member]
        AppUtils.getAppSession()[AUTHORIZED]= auth
    }

    def login(String email, String password) {
        password = password.encodeAsSHA256()
        boolean authorized = false
        Member member = Member.findByEmailAndPassword(email, password)
        if (member) {
            setMemberAuthorization(member)
            authorized = true
        }
        return authorized
    }

    boolean isAuthenticated(){

        def authorization = AppUtils.getAppSession()[AUTHORIZED]
        def authenticated = false

        if(authorization && authorization.isLoggedin){
            authenticated = true
        }
        return authenticated
    }

    def getMember(){
        def authorization = AppUtils.getAppSession()[AUTHORIZED]
        return  authorization?.member
    }

    def getMemberName(){
        def member = getMember()
        return "${member.firstName} ${member.lastName}"
    }

    def isAdmin() {
        def member = getMember()
        def isAdmin = false
        if(member && member.memberType == GlobalConfig.USER_TYPE.ADMINISTRATOR){
            isAdmin = true
        }
        return isAdmin
    }
}
