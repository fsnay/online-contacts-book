package com.fsnay

import com.fsnay.gt.GlobalConfig
import com.fsnay.gt.Member


class AppInitService {

    static initialize(){
        initMember()
    }

    private static initMember(){
        if(Member.count() == 0){
            Member member = new Member()
            member.firstName = "System"
            member.lastName = "Admin"
            member.email = "admin@domain.com"
            member.password = "123456"
            member.memberType = GlobalConfig.USER_TYPE.ADMINISTRATOR
            member.save(flash:true)
        }
    }
}
