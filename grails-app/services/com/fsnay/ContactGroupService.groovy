package com.fsnay

import com.fsnay.gt.AppUtils
import com.fsnay.gt.ContactGroup
import com.fsnay.gt.GlobalConfig
import grails.web.servlet.mvc.GrailsParameterMap


class ContactGroupService {

    AuthService authService

    def get(Serializable id){
        return ContactGroup.get(id)
    }

    def save(def params){
        ContactGroup contactGroup = new ContactGroup(params)
        contactGroup.member = authService.getMember()
        def response = AppUtils.saveResponse(false,contactGroup)
        if(contactGroup.validate()){
            contactGroup.save(flush:true)
            response.isSuccess = true
        }
        return response
    }

    def update(ContactGroup contactGroup, GrailsParameterMap params){
        contactGroup.properties = params
        def response = AppUtils.saveResponse(false,contactGroup)
        if(contactGroup.validate()){
            contactGroup.save(flush:true)
            response.isSuccess = true
        }
        return response
    }

    def list(GrailsParameterMap params){
        params.max = params.max?: GlobalConfig.itensPerPage()
        List<ContactGroup> contactGroupList = ContactGroup.createCriteria().list(params){
            if(params?.colName && params?.colValue){
                like(params.colName, "%" + params.colValue + "%")
            }
            if(!params.sort){
                order("id","desc")
            }
            eq("member",authService.getMember())
        }
        return [list: contactGroupList, count: contactGroupList.totalCount]
    }

    def delete(ContactGroup contactGroup){
        def response = AppUtils.saveResponse(false,contactGroup)
        try{
            cleanGroupContactById(contactGroup.id)
            contactGroup.delete(flush:true)
            response.isSuccess = true
        }
        catch (Exception e){
            println(e.message)
            response.isSuccess = false
        }
        return response
    }

    def getGroupList(){
        return ContactGroup.createCriteria().list{
            eq("member",authService.getMember())
        }
    }

    def cleanGroupContactById(Integer id){
        ContactGroup contactGroup = ContactGroup.get(id)
        contactGroup.contacts.each {contact ->
            contact.removeFromContactGroups(contactGroup)
        }
        contactGroup.save(flush:true)
    }
}
