package com.fsnay

import com.fsnay.gt.AppUtils
import com.fsnay.gt.Contact
import com.fsnay.gt.FileUtils
import com.fsnay.gt.GlobalConfig
import grails.web.servlet.mvc.GrailsParameterMap

import javax.servlet.http.HttpServletRequest


class ContactService {

    AuthService authService
    ContactDetailsService contactDetailsService

    def get(Serializable id){
        return Contact.get(id)
    }

    def list(GrailsParameterMap params) {
        params.max = params.max ?: GlobalConfig.itensPerPage()
        List<Contact> contacts = Contact.createCriteria().list(params) {
            if (params?.colName && params?.colValue) {
                like(params.colName, "%" + params.colValue + "%")
            }
            if (!params.sort) {
                order("id", "desc")
            }
            eq("member", authService.getMember())
        }
        return [list: contacts, count: contacts.totalCount]
    }

    def save(GrailsParameterMap params, HttpServletRequest request){
        Contact contact = new Contact(params)
        contact.member = authService.getMember()
        def response = AppUtils.saveResponse(false,contact)
        if(contact.validate()){
            contact.save(flush:true)
            if(!contact.hasErrors()){
                response.isSuccess = true

                contactDetailsService.createOrUpdateDetails(contact,params)
                uploadImg(contact,request)
            }
        }
        return response
    }

    def update(Contact contact,GrailsParameterMap params, HttpServletRequest request){
        contact.properties = params
        def response = AppUtils.saveResponse(false,contact)
        if(contact.validate()){
            contact.save(flush:true)
            if(!contact.hasErrors()){
                response.isSuccess = true
                contactDetailsService.createOrUpdateDetails(contact,params)
                uploadImg(contact,request)
            }
        }
        return response

    }

    def delete(Contact contact){
        String img = contact.img
        Integer id = contact.id
        def success = false
        try {
            contact.delete(flush: true)
            if (img != null && img != "") {
                success = FileUtils.deleteContactImage(id, img)
            } else {
                success = true
            }
        }
        catch (Exception e){
            log.error(e.message)
            success = false
        }
        return success
    }

    def uploadImg(Contact contact,HttpServletRequest request){
        if (request.getFile("contactImg") && !request.getFile("contactImg").filename.equals("")) {
            String img = FileUtils.uploadContactImage(contact.id, request.getFile("contactImg"))
            if (!img.equals("")) {
                contact.img = img
                contact.save(flush: true)
                if(contact.hasErrors()){
                    println(contact.errors)
                }
            }
        }
    }
}
