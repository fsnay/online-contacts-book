<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 18:48
--%>

<meta name="layout" content="main"/>

<div class="card">
    <div class="card-header">
        <g:message code="title.contacts.group" args="['Update']"/>
    </div>

    <div class="card-body">
        <g:form controller="contactGroup" action="update">
            <g:hiddenField name="id" value="${contactGroup.id}"/>
            <g:render template="form" model="contactGroup"/>
            <div class="form-action-panel">
                <g:submitButton class="btn btn-dark" name="save" value="${g.message(code: "update")}"/>
                <g:link class="btn btn-outline-dark" controller="contactGroup" action="index">
                    <g:message code="cancel"/>
                </g:link>
            </div>
        </g:form>
    </div>
</div>