<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 17:42
--%>

<meta name="layout" content="main">

<div class="card border-light">
    <div class="card-header">
        <g:message code="contacts.group" args="['List Of']"/>
        <span class="float-right">
            <div class="btn-group">
                <g:form controller="contactGroup" action="index" method="GET">
                    <div class="input-group" id="search-area">
                        <g:select class="form-control" name="colName"  value="${params?.colName}" optionKey="key"
                                  from="[name:'Name']" optionValue="value"/>
                        <g:textField class="form-control" name="colValue" value="${params?.colValue}"/>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Search</button>
                        </span>
                    </div>
                </g:form>
            </div>
        </span>
    </div>

    <div class="card-body">
        <span class="float-right">
            <div class="btn btn-group">
                <g:link class="btn btn-dark" controller="contactGroup" action="create"><g:message code="add"/></g:link>
            </div>
        </span>
        <table class="table table-hover">
            <thead>
            <tr>
                <g:sortableColumn property="name" title="${g.message(code:'name')}"/>
                <g:sortableColumn property="member" title="${g.message(code:'title.member')}"/>
                <th class="action-row"><g:message code="action"/> </th>
            </tr>
            </thead>

            <tbody>
                <g:each in="${contactGroupsList}" var="group">
                    <tr>
                        <td>${group?.name}</td>
                        <td><g:each in='${contactGroupsList?.member?}' var="member">
                                ${member.firstName ? member.firstName : 'No one' + '<br>'}
                            </g:each>
                        </td>
                        <td>
                            <div class="btn btn-group">
                                <g:link class="btn btn-dark" controller="contactGroup" action="details" id="${group.id}"><i class="fas fa-eye"></i></g:link>
                                <g:link class="btn btn-outline-dark" controller="contactGroup" action="edit" id="${group.id}"><i class="fas fa-edit"></i></g:link>
                                <g:link class="btn btn-dark delete-confirmation" controller="contactGroup" action="delete" id="${group.id}"><i class="fas fa-trash"></i></g:link>
                            </div>
                        </td>
                    </tr>
                </g:each>
            </tbody>
        </table>
        <div class="paginate">
            <g:paginate total="${total ?: 0}"/>
        </div>
    </div>
</div>