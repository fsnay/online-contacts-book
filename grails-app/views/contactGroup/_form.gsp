<div class="form-group">
    <label><g:message code="name"/>* </label>
    <g:textField class="form-control" name="name" value="${contactGroup?.name}"
                 placeholder="Group name"/>
    <UIHelper:errorMessage fieldName="name" model="${contactGroup}" errorMessage="Required*"/>
</div>

