<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 18:53
--%>

<meta name="layout" content="main"/>

<div class="card">
    <div class="card-header">
        <g:message code="title.contacts.group" args="['Details']"/>
    </div>

    <div class="card-body">
        <g:if test="${contactGroup}">
            <table class="table">
                <tr>
                    <th class="text-right"><g:message code="name"/></th>
                    <td class="text-left">${contactGroup.name}</td>
                </tr>
            </table>
        </g:if>
        <div class="form-action-panel">
            <g:link class="btn btn-outline-dark" controller="contactGroup" action="index">
                <g:message code="cancel"/>
            </g:link>
        </div>
    </div>
</div>