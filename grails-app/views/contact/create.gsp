<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 19:12
--%>

<meta name="layout" content="main"/>

<div class="card">
    <div class="card-header">
        <g:message code="title.contacts" args="['Create']"/>
    </div>

    <div class="card-body">
        <g:form controller="contact" action="save" enctype="multipart/form-data">
            <g:render template="form"/>
            <div class="form-action-panel">
                <g:submitButton class="btn btn-dark" name="save" value="${g.message(code: "save")}"/>
                <g:link class="btn btn-outline-dark" controller="contact" action="index">
                    <g:message code="cancel"/>
                </g:link>
            </div>
        </g:form>
    </div>
</div>