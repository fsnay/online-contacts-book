<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 19:10
--%>

<meta name="layout" content="main">

<div class="card border-light">
    <div class="card-header">
        <g:message code="contacts" args="['List Of']"/>
        <span class="float-right">
            <div class="btn-group">
                <g:form controller="contact" action="index" method="GET">
                    <div class="input-group" id="search-area">
                        <g:select class="form-control" name="colName"  value="${params?.colName}" optionKey="key"
                                  from="[name:'Name']" optionValue="value"/>
                        <g:textField class="form-control" name="colValue" value="${params?.colValue}"/>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Search</button>
                        </span>
                    </div>
                </g:form>
            </div>
        </span>
    </div>

    <div class="card-body">
        <span class="float-right">
            <div class="btn btn-group">
                <g:link class="btn btn-dark" controller="contact" action="create"><g:message code="add"/></g:link>
            </div>
        </span>
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="action-row"><g:message code="thumb"/> </th>
                <g:sortableColumn property="name" title="${g.message(code:'name')}"/>
                <th class="action-row"><g:message code="action"/> </th>
            </tr>
            </thead>

            <tbody>
                <g:each in="${contacts}" var="contact">
                    <tr>
                        <td>
                            <g:if test="${contact.img}">
                                <img src="${resource(dir: "contact-image", file: "/${contact.id}-${contact.img}")}" class="img-thumbnail" style="height: 50px; width: 50px;"/>
                            </g:if>
                            <g:else>
                                <g:img dir="images" file="grails.svg" class="img-thumbnail" style="height: 50px; width: 50px;"/>
                            </g:else>
                        </td>
                        <td>${contact?.name}</td>
                        <td>
                            <div class="btn btn-group">
                                <g:link class="btn btn-dark" controller="contact" action="details" id="${contact.id}"><i class="fas fa-eye"></i></g:link>
                                <g:link class="btn btn-outline-dark" controller="contact" action="edit" id="${contact.id}"><i class="fas fa-edit"></i></g:link>
                                <g:link class="btn btn-dark delete-confirmation" controller="contact" action="delete" id="${contact.id}"><i class="fas fa-trash"></i></g:link>
                            </div>
                        </td>
                    </tr>
                </g:each>
            </tbody>
        </table>
        <div class="paginate">
            <g:paginate total="${total ?: 0}"/>
        </div>
    </div>
</div>