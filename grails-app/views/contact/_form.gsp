<div class="form-group">
    <label><g:message code="name"/>* </label>
    <g:textField class="form-control" name="name" value="${contact?.name}"
                 placeholder="Contact name"/>
    <UIHelper:errorMessage fieldName="name" model="${contact}" errorMessage="Required*"/>
</div>

<div class="form-group">
    <label><g:message code="image"/></label>
    <g:field name="contactImg" class="form-control" type="file" placeholder="Upload Image"/>
    <g:if test="${contact?.img}">
        <img src="${resource(dir: "contact-image", file: "/${contact.id}-${contact.img}")}" class="img-thumbnail" style="margin-top: 10px; height: 100px; width: 100px;"/>
    </g:if>
</div>

<div class="form-group">
    <label><g:message code="contacts.group.name"/></label>
    <UIHelper:contactGroup value="${contact?.contactGroups*.id}" />
</div>

<div class="details-panel">
    <g:include controller="contactDetails" action="create" id="${contact?.id}"/>
</div>



