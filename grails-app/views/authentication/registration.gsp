<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 03:05
--%>

<meta name="layout" content="public">

<div class="card">
    <div class="card-header">
        Member Registration
    </div>
    <div class="card-body">
        <g:form controller="authentication" action="register">
            <g:render template="/member/form"/>
            <g:submitButton class="btn btn-primary" name="registration" value="Registration"/>
            <g:link class="btn btn-primary" controller="authentication" action="login"><g:message code="cancel"/></g:link>
        </g:form>
    </div>
</div>