<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 00:45
--%>

<meta name="layout" content="public" />

<div id="global-wrapper">
    <div id="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 mx-auto">
                    <h1 class="text-center login-title">ONLINE CONTACTS BOOK</h1>
                    <div class="account-wall">
                        <g:img class="profile-img"  dir="images" file="grails.svg" />
                        <g:form class="form-signin" controller="authentication" action="doLogin">
                            <g:textField class="form-control" name="email" required="required" placeholder="E-mail"/>
                            <g:passwordField class="form-control" name="password" required="required" placeholder="Password"/>
                            <g:submitButton class="btn btn-lg btn-primary btn-block" name="login" value="Login"/>
                            <g:link class="btn btn-lg btn-dark btn-block" controller="authentication" action="registration">
                                <g:message code="new.member.register"/>
                            </g:link>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>