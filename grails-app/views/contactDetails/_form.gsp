<div class="form-group">
    <div class="form-inline phone-number-area">

        <g:hiddenField name="detailsId" value="${details ? details.id : ''}"/>

        <div class="form-group">
            <UIHelper:contactType value="${details?.type}"/>
        </div>
        <div class="form-group mx-sm-3">
            <g:textField class="form-control" name="mobile"  value="${details?.mobile}" placeholder="Mobile number"/>
        </div>
        <div class="form-group mx-sm-3">
            <g:textField class="form-control" name="phone" value="${details?.phone}" placeholder="Phone number"/>
        </div>
        <div class="form-group mx-sm-3">
            <g:textField class="form-control" name="email" value="${details?.email}" placeholder="E-mail"/>
        </div>
        <div class="form-group mx-sm-3">
            <g:textField class="form-control" name="website" value="${details?.website}" placeholder="Website"/>
        </div>
        <div class="form-group mx-sm-3">
            <g:textField class="form-control" name="address" value="${details?.address}" placeholder="Address"/>
        </div>

        <g:if test="${details}">
            <button type="button" data-id="${details?.id}" class="btn btn-danger remove-number"><i class="fas fa-trash"></i></button>
        </g:if>
        <g:else>
            <button type="button" class="btn btn-primary add-new-number"><i class="fas fa-plus-circle"></i></button>
        </g:else>
    </div>
</div>