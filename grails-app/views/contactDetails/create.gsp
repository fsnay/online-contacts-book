<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 10/10/2020
  Time: 12:14
--%>

<g:each in="${contactDetails}" var="details">
    <g:render template="form" model="[details: details]"/>
</g:each>
<g:render template="form"/>