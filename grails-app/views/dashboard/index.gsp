<%--
  Created by IntelliJ IDEA.
  User: NFarias
  Date: 09/10/2020
  Time: 01:06
--%>

<html>
<head>
    <meta name="layout" content="main"/>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-5 font-weight-light">Welcome, <span class="font-italic ">${memberName} </span></h1>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body shadow rounded">
                                <g:link class="alert-dark" controller="contact" action="index">
                                    <h3 class="card-title">Contacts</h3>
                                </g:link>
                                <p class="card-text">${totalContacts} contacts added</p>
                                <g:link class="btn btn-dark float-right" controller="contact" action="create"> New contact</g:link>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body shadow rounded"">
                            <g:link class="alert-dark" controller="contactGroup" action="index">
                                <h3 class="card-title">Groups</h3>
                            </g:link>
                            <p class="card-text">${totalGroups} groups added</p>
                            <g:link class="btn btn-dark float-right" controller="contactGroup" action="create"> New group</g:link>
                        </div>
                    </div>
                </div>

            </div>
            </div>

    </div>

</body>
</html>