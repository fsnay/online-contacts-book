package com.fsnay.gt

import com.fsnay.ContactService

class ContactController {

    ContactService contactService

    def index() {
        def response = contactService.list(params)
        [contacts:response.list,total:response.count]
    }

    def create(){
        [contact:flash.redirectParams]
    }

    def edit(Integer id){
        if(flash.redirectParams){
            [contact:flash.redirectParams]
        }
        else{
            def response = contactService.get(id)
            if(!response){
                flash.message = AppUtils.infoMessage(g.message(code:"edit.create.contact.error"),false)
                redirect(controller:"contact",action: "index")
            }
            else{
                [contact:response]
            }
        }
    }

    def details(Integer id){
        def response = contactService.get(id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code: "edit.create.contact.error"),false)
            redirect(controller: "contact",action: "index")
        }
        else{
            [contact: response]
        }
    }

    def save(){
        def response = contactService.save(params,request)
        if(response.isSuccess){
            flash.message = AppUtils.infoMessage(g.message(code:"save.contact.success"))
            redirect(controller: "contact",action: "index")
        }
        else{
            flash.redirectParams = response.model
            flash.message = AppUtils.infoMessage(g.message(code: "edit.create.contact.error"),false)
            redirect(controller: "contact",action: "create")
        }
    }

    def update(){
        def response = contactService.get(params.id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code: "edit.create.contact.error"),false)
            redirect(controller: "contact",action: "index")
        }
        else{
            response = contactService.update(response, params,request)
            if(!response){
                flash.redirectParams = response.model
                flash.message = AppUtils.infoMessage(g.message(code: "edit.create.contact.error"),false)
                redirect(controller: "contact",action: "edit")
            }
            else{
                flash.message = AppUtils.infoMessage(g.message(code:"edit.contact.success"))
                redirect(controller: "contact",action: "index")
            }
        }
    }

    def delete(Integer id){
        def response = contactService.get(id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code: "edit.create.contact.error"),false)
            redirect(controller: "contact", action: "index")
        }
        else{
            response = contactService.delete(response)
            if(!response){
                flash.message = AppUtils.infoMessage(g.message(code: "delete.contact.error"),false)
            }
            else{
                flash.message = AppUtils.infoMessage(g.message(code: "delete.contact.success"))
            }
            redirect(controller: "contact",action: "index")
        }
    }

}
