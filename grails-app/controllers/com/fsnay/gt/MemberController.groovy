package com.fsnay.gt

import com.fsnay.MemberService

class MemberController {

    MemberService memberService

    def index() {
        def response = memberService.list(params)
        [memberList:response.list,total:response.count]
    }

    def details(Integer id){
        def response = memberService.getById(id)
        if(!response){
            redirect(controller:"member",action: "index")
        }
        else{
            [member:response]
        }
    }

    def create(){
        [member: flash.redirectParams]
    }

    def edit(Integer id){
        if(flash.redirectParams){
            [member: flash.redirectParams]
        }
        else{
            def response = memberService.getById(id)
            if(!response){
                flash.message = AppUtils.infoMessage(g.message(code:"edit.create.member.error"),false)
                redirect(controller: "member", action:"index")
            }
            else{
                [member: response]
            }
        }
    }

    def save(){
        def response = memberService.save(params)

        if(!response.isSuccess){
            flash.redirectParams = response.model
            flash.message = AppUtils.infoMessage(g.message(code:"edit.create.member.error"),false)
            redirect(controller: "member",action: "create")
        }
        else{
            flash.message = AppUtils.infoMessage(g.message(code:"save.member.success"))
            redirect(controller: "member", action: "index")
        }
    }

    def update(){
        def response = memberService.getById(params.id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code:"edit.create.member.error"),false)
            redirect(controller: "member",action:"index")
        }
        else{
            response = memberService.update(response,params)
            if(!response.isSuccess){
                flash.redirectParams = response.model
                flash.message = AppUtils.infoMessage(g.message(code:"edit.create.member.error"),false)
                redirect(controller: "member",action:"edit")
            }
            else{
                flash.message = AppUtils.infoMessage(g.message(code:"edit.member.success"))
                redirect(controller: "member",action: "index")
            }
        }

    }
    def delete(Integer id){
        def response = memberService.getById(id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code:"edit.create.delete.error"),false)
            redirect(controller: "member",action: "index")
        }
        else{
            response = memberService.delete(response)
            if(!response){
                flash.message = AppUtils.infoMessage(g.message(code:"delete.member.error"),false)
            }
            else{
                flash.message = AppUtils.infoMessage(g.message(code:"delete.member.success"))
            }
            redirect(controller: "member",action: "index")
        }
    }
}
