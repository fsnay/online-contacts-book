package com.fsnay.gt

import com.fsnay.ContactDetailsService
import com.fsnay.ContactService
import grails.converters.JSON

class ContactDetailsController {

    ContactDetailsService contactDetailsService
    ContactService contactService

    def create(){
        Contact contact = contactService.get(params.id)
        def contactDetails = contactDetailsService.getContactDetailsByContact(contact)
        [contactDetails:contactDetails]
    }

    def delete(Integer id){
        render(contactDetailsService.delete(id) as JSON)
    }
}
