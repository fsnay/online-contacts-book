package com.fsnay.gt

import com.fsnay.AuthService


class SecurityInterceptor {

    AuthService authService

    SecurityInterceptor(){
        matchAll().excludes(controller:"authentication")
    }

    boolean before() {

        if(!authService.isAuthenticated()){
            redirect(controller: "authentication",action: "login")
            return false
        }
        return true
    }

}
