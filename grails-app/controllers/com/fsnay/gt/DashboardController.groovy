package com.fsnay.gt

import com.fsnay.AuthService
import com.fsnay.ContactGroupService
import com.fsnay.ContactService

class DashboardController {

    AuthService authService
    ContactGroupService contactGroupService
    ContactService contactService

    def index() {
        def memberName = authService.getMemberName()
        def totalContacts = contactService.list(params).count
        def totalGroups = contactGroupService.list(params).count
        [memberName:memberName,totalContacts:totalContacts,totalGroups:totalGroups]
    }
}
