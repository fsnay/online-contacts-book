package com.fsnay.gt

import com.fsnay.AuthService


class MemberInterceptor {

    AuthService authService

    boolean before() {
        def isAdmin = false
        if(authService.isAdmin()){
            isAdmin = true
        }
        else{
            flash.message = AppUtils.infoMessage("Authorization required",false)
            redirect(controller:"dashboard",action:"index")
        }
        return isAdmin

    }
}
