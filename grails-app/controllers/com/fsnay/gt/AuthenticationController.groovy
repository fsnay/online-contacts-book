package com.fsnay.gt

import com.fsnay.AuthService
import com.fsnay.MemberService

class AuthenticationController {

    AuthService authService
    MemberService memberService

    def login(){
        if(authService.isAuthenticated()){
            redirect(controller:"dashboard",action:"index")
        }
    }

    def doLogin(){
        if(authService.login(params.email,params.password)){
            redirect(controller:"dashboard",action:"index")
        }
        else{
            flash.message = AppUtils.infoMessage("Invalid E-mail or Password", false)
            redirect(controller:"authentication",action:"login")
        }
    }

    def logout(){
        session.invalidate()
        redirect(controller: "authentication",action:"login")
    }

    def registration(){
        [member:flash.redirectParams]
    }

    def register(){
        def response = memberService.save(params)
        if(response.isSuccess){
            authService.setMemberAuthorization(response.model)
            redirect(controller: "dashboard", action: "index")
        }
        else{
            flash.redirectParams = response.model
            redirect(controller: "authentication", action: "registration")
        }
    }
}
