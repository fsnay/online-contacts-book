package com.fsnay.gt

import com.fsnay.ContactGroupService

class ContactGroupController {

    ContactGroupService contactGroupService

    def index(){
        def response = contactGroupService.list(params)
        println('contact group ' + response)
        [contactGroupsList:response.list, total: response.count]
    }

    def create(){
        [contactGroup:flash.redirectParams]
    }

    def edit(Integer id){
        if(flash.redirectParams){
            [contactGroups: flash.redirectParams]
        }
        else{
            def response = contactGroupService.get(id)
            if(!response){
                flash.message = AppUtils.infoMessage(g.message(code:"edit.create.contact.group.error"),false)
                redirect(controller: "contactGroup",action:"index")
            }
            else{
                [contactGroup:response]
            }
        }
    }

    def details(Integer id){
        def response = contactGroupService.get(id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code: "edit.create.contact.group.error"),false)
            redirect(controller:"contactGroup",action:"index")
        }
        else{
            [contactGroup: response]
        }
    }

    def save(){
        def response = contactGroupService.save(params)
        if(response.isSuccess){
            flash.message = AppUtils.infoMessage(g.message(code:"save.contact.group.success"))
            redirect(controller:"contactGroup",action: "index")
        }
        else{
            flash.redirectParams = response.model
            flash.message = AppUtils.infoMessage(g.message(code:"edit.create.contact.group.error"),false)
            redirect(controller: "contactGroup", action: "create")
        }
    }

    def update(){
        def response = contactGroupService.get(params.id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code:"edit.create.contact.group.error"),false)
            redirect(controller: "contactGroup",action: "index")
        }
        else{
            response = contactGroupService.update(response,params)
            if(!response){
                flash.redirectParams = response.model
                flash.message = AppUtils.infoMessage(g.message(code:"edit.create.contact.group.error"),false)
                redirect(controller: "contactGroup", action: "edit")
            }
            else{
                flash.message = AppUtils.infoMessage(g.message(code:"edit.contact.group.success"))
                redirect(controller: "contactGroup", action: "index")
            }
        }
    }

    def delete(Integer id){
        def response = contactGroupService.get(id)
        if(!response){
            flash.message = AppUtils.infoMessage(g.message(code:"edit.create.contact.group.error"),false)
            redirect(controller: "contactGroup",action: "index")
        }
        else{
            response = contactGroupService.delete(response)
            if(!response){
                flash.message = AppUtils.infoMessage(g.message(code:"delete.contact.group.error"),false)
            }
            else{
                flash.message = AppUtils.infoMessage(g.message(code:"delete.contact.group.success"))
            }
            redirect(controller: "contactGroup",action: "index")

        }
    }
}
