package com.fsnay.gt

class ContactGroup {

    Integer id
    String name
    Member member

    Date dateCreated
    Date lastUpdated

    static constraints = {
        name(nullable: false,blank: false)
        member(nullable: true)
    }

    static hasMany = [contacts: Contact]
    static belongsTo = [Contact]

    static mapping = {
        version(false)
    }
}
