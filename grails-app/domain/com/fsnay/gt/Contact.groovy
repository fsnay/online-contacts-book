package com.fsnay.gt

class Contact {

    Integer id
    String name
    String img
    Member member

    Date dateCreated
    Date lastUpdated

    Set<ContactDetails> contactDetails
    Set<ContactGroup> contactGroups

    static constraints = {
        name(nullable: false,blank: false)
        img(nullable: true,blank: true)
    }

    static hasMany = [contactDetails:ContactDetails,contactGroups:ContactGroup]

    static mapping = {
        version(false)
        contactDetails(cascade: 'all-delete-orphan')
    }


}
