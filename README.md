# Grails Online Contacts Book

This project is an online contacts book created by following [hmtmcse grails tutorial] [hmtmcse].  This is also being used as an evaluation process in a job application.

### Technologies

* [Grails] - version 3.3.9
* [Java 8] - jdk and jre version 8u261
* [Bootstrap] - version 4.0.0
* [jQuery] - version 3.3.1
* [jQuery confirm plugin]  - version 3.3.4
* [Intellij] - IDE, version 2020.2.3


### Installation

The project requires [java 8] and its system variable (JAVA_HOME or JRE_HOME) must be configured to run.


You can download download the project .war file to run it in a local server like tomcat. 
[Download .war file][project war]

You can also [clone or download the project][project location] and run it with your preferred IDE that supports grails. In this case, just import the project and once it finishes downloading dependencies, you can run the project in a local server for java environments (wildfly, tomcat, glassfish etc). It will be available in your localhost then.


### Explore the application

In this project, you will be able to create contacts with detailed information contact groups for them. You can also register new members if you login as Administrator (information bellow).

You will need to be registered as member to access the dashboard and those features. For that, it's possible to both create a new account or Login with admin credentials: 
email: admin@domain.com
password: 123456


   [hmtmcse]: <https://github.com/hmtmcse-com/grails-tutorial-contacts-book/>
   [grails]: <https://grails.org/>
   [bootstrap]: <https://getbootstrap.com/>
   [jQuery]: <http://jquery.com>
   [java 8]: <https://www.oracle.com/br/java/technologies/javase-downloads.html#JDK8>
   [jquery confirm plugin]: <https://craftpip.github.io/jquery-confirm/>
   [intellij]: <https://www.jetbrains.com/idea/>
   [project war]: <https://shorturl.at/lnwGW>
   [project location]: <https://gitlab.com/fsnay/online-contacts-book>
