package com.fsnay.gt

import grails.util.Holders
import org.springframework.web.multipart.MultipartFile

class FileUtils {
    public static String getRootPath(){
        return Holders.servletContext?.getRealPath("/")
    }


    public static File makeDir(String path){
        File file = new File(path)
        if (!file.exists()){
            file.mkdirs()
        }
        return file
    }

    public static String uploadContactImage(Integer contactId, MultipartFile multipartFile){
        if (contactId && multipartFile){
            String contactImagePath = "${getRootPath()}contact-image/"
            makeDir(contactImagePath)
            multipartFile.transferTo(new File(contactImagePath, contactId + "-" + multipartFile.originalFilename))

            println(contactImagePath)
            return multipartFile.originalFilename
        }
        return ""
    }

    public static deleteContactImage(Integer contactId, String originalName){
        String imgName = contactId + '-'+ originalName
        String path = FileUtils.getRootPath() + '/contact-image/' + imgName
        println(path)

        Boolean isSuccess = false

        File file = new File(path)
        if(file){
            isSuccess = file.delete()
        }
        return isSuccess
    }

}
